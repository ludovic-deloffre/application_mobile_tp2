package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel {

    private BookRepository repository;

    private MutableLiveData<Book> searchBook;

    private Book newBook;

    static long currentId = 11;

    public DetailViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        searchBook = repository.getSelectedBook();
        this.newBook = newBook;
    }

    public void setSearchBook(long id) {
        repository.findBook(id);
        searchBook = repository.getSelectedBook();
    }

    public MutableLiveData<Book> getSearchBook() {
        return searchBook;
    }


    public void insertOrUpdateBook(Book book){

        if ( (book.getId()!=-1)) {
//            searchBook.getValue().setTitle(title);
//            searchBook.getValue().setAuthors(auteur);
//            searchBook.getValue().setYear(year);
//            searchBook.getValue().setGenres(genre);
//            searchBook.getValue().setPublisher(editeur);

            //repository.updateBook(searchBook.getValue());
            repository.updateBook(book);
        }
        else{

                currentId += 1;
                book.setId(currentId);
                repository.insertBook(book);
        }


    }

}






















